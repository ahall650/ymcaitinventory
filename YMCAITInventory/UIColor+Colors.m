//
//  UIColor+Colors.m
//  GPS Mileage
//
//  Created by anthony hall on 11/10/13.
//  Copyright (c) 2013 anthony hall. All rights reserved.
//

#import "UIColor+Colors.h"
#import "ThemeManager.h"

@implementation UIColor (Colors)

+ (UIColor *)themeColorNamed:(NSString *)key{
    NSDictionary *styles = [ThemeManager sharedThemeManager].styles;
    NSString *labelColor = [styles objectForKey:key];
    return [UIColor colorFromHexString:labelColor];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
   // [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
/*
+(UIColor *)paleGreenColor{
    return [UIColor colorWithRed:211.0/255.0f green:255.0/255.0f blue:202.0/255.0f alpha:1];
   // return [UIColor colorWithRed:185.0/255.0f green:213.0/255.0f blue:225.0/255.0f alpha:1];

}
+(UIColor *)palePinkColor{
    return [UIColor colorWithRed:226.0/255.0f green:203.0/255.0f blue:220.0/255.0f alpha:1];
    //return [UIColor colorWithRed:227.0/255.0f green:239.0/255.0f blue:241.0/255.0f alpha:1];

    
}
+(UIColor *)darkGrayText{
    return [UIColor colorWithRed:102/255.0 green:102/255.0 blue:102/255.0 alpha:1];
}
+(UIColor *)darkGreen{
    
    return [UIColor colorWithRed:117/255.0 green:134/255.0 blue:78/255.0 alpha:1];
    //return [UIColor colorWithRed:103/255.0 green:120/255.0 blue:154/255.0 alpha:1];

}

+(UIColor *)darkGreenBarColor{
    return [UIColor colorWithRed:81/255.0 green:93/255.0 blue:54/255.0 alpha:1];
    
}
+(UIColor *)darkGreenCell{
    return [UIColor colorWithRed:82/225.0 green:92/225.0 blue:54/255.0 alpha:1];
}
+(UIColor *)blackTransparent{
    return [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:.30];
}
 */
@end
