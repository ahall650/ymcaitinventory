//
//  DataModel.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/18/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InventoryItem.h"
#import "NSString+customStatusCompare.h"

typedef void (^ITCompletionBlock) ();


@interface DataModel : NSObject
@property(nonatomic, strong)NSArray *sortedCategories;
@property(nonatomic, strong)NSMutableDictionary *sections;

-(void)refreshData:(ITCompletionBlock)completion;
-(void)getLocalData;
-(void)insertNewItem:(InventoryItem *)item completion:(void(^)(BOOL))success;
-(void)checkForDuplicateItem:(InventoryItem *)item completion:(void(^)(BOOL))completion;
-(void)updateItem:(InventoryItem *)item withCompletion:(ITCompletionBlock)completion;
-(void)deleteItemWithIDString:(NSString *)string andCompletion:(ITCompletionBlock)completion;
-(void)checkoutItemWithDictionary:(NSDictionary *)dict withCompletion:(ITCompletionBlock)completion;
-(InventoryItem *)getItemFromQRData:(InventoryItem *)item;
-(NSInteger)sectionChangeFromItem:(InventoryItem *)item;
-(void)sortCategoryDataWithKey:(NSString *)key;

@end
