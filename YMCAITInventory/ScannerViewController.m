//
//  ScannerViewController.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/21/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "ScannerViewController.h"
#import "QRReader.h"
#import "Constant.h"
#import "ITAlertView.h"
@interface ScannerViewController ()
@property (weak, nonatomic) IBOutlet UIView *cameraPreviewView;

@end

@implementation ScannerViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    //_theItem = [InventoryItem new];
    self.qrReader = [[QRReader alloc]init];
    _qrReader.delegate = self;
    _listController.delegate = self;
    [_qrReader setupScanningSession];
    [_qrReader.captureSession startRunning];
    _containerView.backgroundColor = [UIColor themeColorNamed:kMidColor];
    [_actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setupLables];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)completeCheckoutWithOwner:(NSString *)owner{
    _theItem.isCheckedOut = YES;
    _theItem.checkedoutDate = [NSDate date];
    _theItem.owner = owner;
    [_dataModel checkoutItemWithDictionary:[_theItem ItemDictionaryForCheckout] withCompletion:nil];
    [self.listController dismissAfterCheckedStatusUpdate];
}

- (IBAction)rescanButtonPressed:(id)sender {
    [_qrReader.captureSession startRunning];
    [self setupLables];
}
-(void)determineActionButtonFromItem:(InventoryItem *)item{
    [_dataModel checkForDuplicateItem:_theItem completion:^(BOOL isDuplicate){
        _actionButton.hidden = NO;
        NSString *buttonTitle;
        if (isDuplicate == YES) {
            //item is in database, checkin or out
            if (item.isCheckedOut) {
                buttonTitle = @"Check in";
            }else{
                buttonTitle = @"Check out";
            }
        }else{
            //item is new show add button
            buttonTitle = @"Add Item";
        }
        [_actionButton setTitle:buttonTitle forState:UIControlStateNormal];
        [self refreshScannedLabels];
    }];
}
- (IBAction)actionButtonPressed:(id)sender {
    if ([_actionButton.titleLabel.text isEqualToString:@"Check in"]) {
        //checkin
        _theItem.isCheckedOut = NO;
        _theItem.owner = @"none";
        [_dataModel checkoutItemWithDictionary:[_theItem ItemDictionaryForCheckin] withCompletion:nil];
        [self.listController dismissAfterCheckedStatusUpdate];

    }else if([_actionButton.titleLabel.text isEqualToString:@"Check out"]){
        //check out
        [self presentOwnerEntryViewWithMesage:nil];

    }else if([_actionButton.titleLabel.text isEqualToString:@"Add Item"]){
        //add item
        [_dataModel insertNewItem:_theItem completion:^(BOOL success){
            if(success){
                [self.listController dismissModalControllerAfterUpdatesWithItem:_theItem];
            }else{
                NSLog(@"problem INSERTING NEW ITEM");
            }
        }];
    }
}

#pragma mark - QRReader Delegate Methods
-(void)setCaptureLayerFrameWithLayer:(AVCaptureVideoPreviewLayer *)captureLayer{
    [captureLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [captureLayer setFrame:self.cameraPreviewView.layer.bounds];
    [self.cameraPreviewView.layer addSublayer:captureLayer];

}
-(void)didFinishScanning:(NSString *)barcodeText{
    InventoryItem *qrItem = [_qrReader parseBarcodeData:barcodeText];
    _theItem = [_dataModel getItemFromQRData:qrItem];
    [self determineActionButtonFromItem:_theItem];

}
-(void)setupLables{
    UIColor *labelColor = [UIColor whiteColor];
    NSArray *labels = @[_categoryText, _CategoryLabel, _nameText, _nameLabel, _ownerText, _ownerLabel, _isCheckedOutText, _isCheckedOutLabel, _dateText, _dateLabel, _notesLabel];
    for (UILabel *label in labels) {
        label.textColor = labelColor;
    }
    _notesTextview.textColor = labelColor;
    
    _CategoryLabel.text = @"Category:";
    _nameLabel.text = @"Name";
    _ownerLabel.text = @"Owner";
    _isCheckedOutLabel.text = @"Is Checked Out";
    _dateLabel.text = @"Date Checked Out";
    _notesLabel.text = @"Notes";
    
    _categoryText.text = @"";
    _nameText.text = @"";
    _ownerText.text = @"";
    _notesTextview.text = @"";
    _isCheckedOutText.text = @"";
    _dateText.text = @"";
    [_actionButton setTitle:@"" forState:UIControlStateNormal];

    _actionButton.hidden = YES;

}
-(void)refreshScannedLabels{
    _categoryText.text = _theItem.category;
    _nameText.text = _theItem.name;
    _ownerText.text = _theItem.owner;
    _notesTextview.text = _theItem.notes;
    
    if (_theItem.isCheckedOut == NO) {
        _isCheckedOutText.text = @"No";
        _dateText.text = @"";
    }else{
        _isCheckedOutText.text = @"Yes";
        _dateText.text = [InventoryItem convertDateToString:_theItem.checkedoutDate];

    }
}
#pragma -mark alert view controllers
-(void)presentDuplicateError{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Duplicate Item"
                                                                              message: @"Enter a Unique Item Name"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];

}
-(void)presentOwnerEntryViewWithMesage:(NSString *)message{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Checked out to:"
                                                                              message: message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        if ([namefield.text isEqualToString:@""]) {
            [self presentOwnerEntryViewWithMesage:@"Please Enter a Name"];
        }else{
            [self completeCheckoutWithOwner:namefield.text];

        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
   
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}
@end
