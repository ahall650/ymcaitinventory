//
//  UIColor+Colors.h
//  GPS Mileage
//
//  Created by anthony hall on 11/10/13.
//  Copyright (c) 2013 anthony hall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Colors)
/*
+(UIColor *)paleGreenColor;
+(UIColor *)darkGrayText;
+(UIColor *)darkGreen;
+(UIColor *)palePinkColor;
+(UIColor *)darkGreenCell;
+(UIColor *)darkGreenBarColor;
+(UIColor *)blackTransparent;
 */
+ (UIColor *)themeColorNamed:(NSString *)key;
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end
