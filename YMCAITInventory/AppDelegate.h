//
//  AppDelegate.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

