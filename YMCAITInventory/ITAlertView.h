//
//  ITAlertView.h
//  YMCAITInventory
//
//  Created by anthony hall on 4/1/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ITAlertView : NSObject
+(UIAlertController *)alertDuplicate;
@end
