//
//  SyncManager.h
//  YMCAInventory
//
//  Created by anthony hall on 3/11/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MicrosoftAzureMobile/MicrosoftAzureMobile.h>
#import "InventoryItem.h"
typedef void (^ITCompletionBlock) ();


@interface SyncManager : NSObject
+ (SyncManager *) sharedSyncManager;
@property (nonatomic, strong) MSClient *client;
@property (nonatomic, strong) NSArray *data;
-(void)addItem:(NSDictionary *)item completion:(SyncManager *)completion;
-(void)pullData:(ITCompletionBlock)completion;
-(void)queryStringAttribute:(NSString *)attribute andValue:(NSString *)value withCompletion:(void(^)(InventoryItem *)) completion;
-(void)updateItem:(InventoryItem *)item withCompletion:(ITCompletionBlock)completion;
-(void)udpateItemWithDictionary:(NSDictionary *)dict withCompletion:(ITCompletionBlock)completion;
-(void)deleteItemWithIDSting:(NSString *)string completion:(ITCompletionBlock)completion;
@end
