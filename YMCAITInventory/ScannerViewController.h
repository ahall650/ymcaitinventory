//
//  ScannerViewController.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/21/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "QRReader.h"
#import "InventoryItem.h"
#import "DataModel.h"
#import "InventorytListViewController.h"
@interface ScannerViewController : UIViewController <QRReaderDelegate, InventoryListDelegate>

@property (strong, nonatomic)InventorytListViewController *listController;
@property (strong, nonatomic)QRReader * qrReader;
@property (strong, nonatomic)InventoryItem *theItem;
@property (strong, nonatomic)DataModel *dataModel;
- (IBAction)actionButtonPressed:(id)sender;

-(IBAction)rescanButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *actionButton;

//stackview Labels
@property (strong, nonatomic) IBOutlet UILabel *CategoryLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *ownerLabel;
@property (strong, nonatomic) IBOutlet UILabel *isCheckedOutLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *notesLabel;
//stackview textfields
@property (strong, nonatomic) IBOutlet UILabel *categoryText;
@property (strong, nonatomic) IBOutlet UILabel *nameText;
@property (strong, nonatomic) IBOutlet UILabel *ownerText;
@property (strong, nonatomic) IBOutlet UILabel *isCheckedOutText;
@property (strong, nonatomic) IBOutlet UILabel *dateText;
@property (strong, nonatomic) IBOutlet UITextView *notesTextview;

@property (strong, nonatomic) IBOutlet UIView *containerView;

@end

