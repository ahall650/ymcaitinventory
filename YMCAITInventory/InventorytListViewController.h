//
//  MasterViewController.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"
typedef void (^ITCompletionBlock) ();

@class InventoryDetailViewController;
@protocol InventoryListDelegate;

@interface InventorytListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) id<InventoryListDelegate>delegate;

@property (strong, nonatomic) InventoryDetailViewController *inventoryDetailViewController;
@property (strong, nonatomic) IBOutlet UITableView *theTableView;
//dismissal methods
-(IBAction)dismissAnyModal:(UIStoryboardSegue *)sender;
-(IBAction)dismissManualInsertWithSegue:(UIStoryboardSegue *)sender;
-(void)dismissModalControllerAfterUpdatesWithItem:(InventoryItem *)item;
-(void)dismissAfterCheckedStatusUpdate;
- (IBAction)sort:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sortButton;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@protocol InventoryListDelegate <NSObject>


@end
