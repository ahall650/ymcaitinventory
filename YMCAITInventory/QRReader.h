//
//  QRReader.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "InventoryItem.h"
@protocol QRReaderDelegate;


@interface QRReader : NSObject <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, weak) id<QRReaderDelegate>delegate;

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *captureLayer;
-(void)setupScanningSession;
-(InventoryItem *)parseBarcodeData:(NSString *)data;
@end

@protocol QRReaderDelegate <NSObject>

@required
-(void)setCaptureLayerFrameWithLayer:(AVCaptureVideoPreviewLayer *)captureLayer;
-(void)didFinishScanning:(NSString *)barcodeText;
@end
