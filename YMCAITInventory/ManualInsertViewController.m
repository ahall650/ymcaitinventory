//
//  ManualInsertViewController.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/18/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "ManualInsertViewController.h"
#import "Constant.h"
#import "ITAlertView.h"

@interface ManualInsertViewController ()
@property (nonatomic, strong)NSArray *categories;
@end

@implementation ManualInsertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _categories = [self createCategoryList];
    _categoryPicker.delegate = self;
    _categoryPicker.dataSource = self;
    _theItem = [InventoryItem new];
    _theItem.category = _categories[0];
    [self setupLabels];
    
}
-(NSArray *)createCategoryList{
    NSArray *base = @[@"Laptop", @"Chromebook", @"AV", @"Adapter"];
    NSArray *newArray = [base arrayByAddingObjectsFromArray:_dataModel.sortedCategories];
    NSArray *uniqueEntries = [[NSSet setWithArray:newArray] allObjects];

    return [uniqueEntries sortedArrayUsingSelector:@selector(compare:)];
}
-(void)setupLabels{
    _categoryLabel.text = @"Category";
    _itemLabel.text = @"Item Name";
    _notesLabel.text =@"Notes";
    UIColor *labelColor = [UIColor whiteColor];
    NSArray *labels = @[_categoryLabel, _itemLabel, _notesLabel];
    for (UILabel *label in labels) {
        label.textColor = labelColor;
    }
    _notesTextview.textColor = labelColor;
    _notesTextview.backgroundColor = [UIColor themeColorNamed:kLightColor];
    _notesTextview.layer.cornerRadius = 10.0;
    _notesTextview.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    _categoryPicker.backgroundColor = [UIColor themeColorNamed:kDarkColor];
    
    _containerView.backgroundColor = [UIColor themeColorNamed:kMidColor];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == _itemTextField){
        _theItem.name = textField.text;
    }
}
-(void)textViewDidChange:(UITextView *)textView{
    textView.text = _theItem.notes;
}
#pragma mark - Category Picker
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _categories.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = _categories[row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _theItem.category = _categories[row];
}
-(IBAction)validateData:(id)sender{
    if ([_itemTextField.text isEqualToString:@""] || _itemTextField == nil ) {
        //TODO exit without saving anything
    }else{
        _theItem.name = _itemTextField.text;
        _theItem.notes = _notesTextview.text;
        _theItem.IDString = _itemTextField.text; //name and ID are the same
        [_dataModel insertNewItem:_theItem completion:^(BOOL success){
            if(success){
                [self performSegueWithIdentifier:@"manualUnwind" sender:self];
            }else{
                //present error message
                [self presentViewController:[ITAlertView alertDuplicate] animated:YES completion:nil];
            }
            
        }];
    }
}

@end
