//
//  NSString+customStatusCompare.h
//  YMCAITInventory
//
//  Created by anthony hall on 6/10/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (customStatusCompare)
- (NSComparisonResult)customStatusCompare:(NSString*)other;
@end
