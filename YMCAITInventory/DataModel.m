//
//  DataModel.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/18/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "DataModel.h"
#import "SyncManager.h"
#import "InventoryItem.h"
#import "NSString+customStatusCompare.h"
#import "ITAlertView.h"
@interface DataModel ()
@property (nonatomic, strong) SyncManager *syncManager;
@property(nonatomic, strong)NSArray *items;
@property(nonatomic, strong)NSString *sortKey;


@end

@implementation DataModel

#pragma mark - public
//use to pull data from web
-(void)refreshData:(ITCompletionBlock)completion{
    if (_syncManager == nil) {
        _syncManager = [SyncManager sharedSyncManager];

    }
    [_syncManager pullData:^
     {
         NSLog(@"complete");
         [self convertDataToObjects:_syncManager.data];
         [self saveDataToDisk];
         [self catagorizeData];
         if (completion != nil) {
             dispatch_async(dispatch_get_main_queue(), completion);
         }
     }];
}
-(void)insertNewItem:(InventoryItem *)item completion:(void(^)(BOOL))success{
     [self checkForDuplicateItem:item completion:^(BOOL isDuplicate){
         if(!isDuplicate){
             [self insertItemToTable:item];
             [self insertNewItemToLocalDatabase:item];
             [self saveDataToDisk];
         }
         success(!isDuplicate);
     }];
    
}
-(void)getLocalData{
    self.items = [self getDataFromDisk];
    [self catagorizeData];
}
-(InventoryItem *)getItemFromQRData:(InventoryItem *)item{
    NSArray *dataArray = [self getDataFromDisk];
    NSString *key = item.IDString;
    for (InventoryItem *currItem in dataArray) {
        if ([currItem.IDString isEqualToString:key]) {
            
            return currItem;
        }
    }
    return item;
}

-(void)checkForDuplicateItem:(InventoryItem *)item completion:(void(^)(BOOL))completion{
    [_syncManager queryStringAttribute:@"name" andValue:item.name withCompletion:^(InventoryItem *item){
        BOOL isDuplicate = NO;
        if (item == nil) {
            // NSLog(@"not a duplicate");
        }else{
            //NSLog(@"duplicate");
            isDuplicate = YES;
        }
        if (completion != nil) {
            completion(isDuplicate);
        }
        
    }];
    
}
-(NSInteger)sectionChangeFromItem:(InventoryItem *)item{
    NSInteger section = 0;
    NSString *categoryKey = item.category;
    for (int i = 0; i < self.sortedCategories.count; i++) {
        if ([[self.sortedCategories objectAtIndex:i] isEqualToString:categoryKey]) {
            section = (NSInteger) i;
        }
    }
    return section;
}

-(void)updateItem:(InventoryItem *)item withCompletion:(ITCompletionBlock)completion{
    [_syncManager updateItem:item withCompletion:^{
        [self refreshData:^{
            [self postUpdate];
            
        }];
        
    }];
}
-(void)deleteItemWithIDString:(NSString *)string andCompletion:(ITCompletionBlock)completion{

    [_syncManager deleteItemWithIDSting:string completion:^{
       //do stuff when database is updated
        [self refreshData:^{
            //[self postUpdate];
            dispatch_async(dispatch_get_main_queue(), completion);
        }];
    }];
     
}

-(void)checkoutItemWithDictionary:(NSDictionary *)dict withCompletion:(ITCompletionBlock)completion{
    [_syncManager udpateItemWithDictionary:dict withCompletion:^{
        [self refreshData:^{
            [self postUpdate];
            
        }];
        
    }];
    
}

#pragma mark - private
-(void)postUpdate{
    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataUpdated" object:nil];
        NSLog(@"data update notifictation posted");
    });
}
//convert azure table data to InventoryItem Objects
-(void)convertDataToObjects:(NSArray *)data{
    NSMutableArray *temp = [NSMutableArray new];
    for (NSDictionary *dict in data) {
        InventoryItem *item = [[InventoryItem alloc]initWithDictionary:dict];
        [temp addObject:item];
    }
    self.items = temp;
}
-(void)insertNewItemToLocalDatabase:(InventoryItem *)item{
    NSMutableArray *temp = [_items mutableCopy];
    [temp addObject:item];
    _items = temp;
    [self catagorizeData];
}
-(void)insertItemToTable:(InventoryItem *)item{
    NSDictionary *dict = [item itemDictionaryForPosting];
    [_syncManager addItem:dict completion:nil];
}
//sorts data into catagories for grouped table
-(void)catagorizeData{

    self.sections = [NSMutableDictionary new];
    
    for (InventoryItem *item in _items) {
        NSMutableArray *arrayOfCategory = [self.sections objectForKey:item.category];
        if (arrayOfCategory == nil) {
            arrayOfCategory = [NSMutableArray array];
            [self.sections setObject:arrayOfCategory forKey:item.category];
        }
        [arrayOfCategory addObject:item];
    }
    NSArray *unsortedCategories = [self.sections allKeys];
    //remove duplicates
    self.sortedCategories = [unsortedCategories sortedArrayUsingSelector:@selector(compare:)];
    [self sortCategoryDataWithKey:_sortKey];
}

-(void)sortCategoryDataWithKey:(NSString *)key{
    //key is set to to property to save sorting style on table update
    if(key == nil){
        _sortKey = @"IDString";
    }else{
        _sortKey = key;
    }
    for (int i = 0; i < _sortedCategories.count; i++) {
        NSString *currentCatagory = [_sortedCategories objectAtIndex:i];

        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_sortKey
                                                     ascending:YES
                                                     selector:@selector(customStatusCompare:)];
        
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [[_sections objectForKey:currentCatagory] sortedArrayUsingDescriptors:sortDescriptors];
        [_sections setObject:sortedArray forKey:currentCatagory];
    }
}
-(void)saveDataToDisk{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_items];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:@"InventoryItems"];
    [defaults synchronize];
}
-(NSArray *)getDataFromDisk{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *array= [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:@"InventoryItems"]];

    return array;
}


@end

