//
//  DetailViewController.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "InventoryDetailViewController.h"
#import "SyncManager.h"
#import "Constant.h"
@interface InventoryDetailViewController ()

@end

@implementation InventoryDetailViewController

- (void)configureView {
    // Update the user interface for the detail item.
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _notesTextview.delegate = self;
    _backgroundView.backgroundColor = [UIColor themeColorNamed:kMidColor];
     [_actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    _doneButton = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(updateTableWithNotes)];
    self.navigationItem.rightBarButtonItem = _doneButton;
    [self showDoneButton:NO];

    [self setupLables];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    if(_doneButton.isEnabled){
        [self updateTableWithNotes];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showDoneButton:(BOOL)show{
    if(show){
        [_doneButton setEnabled:YES];
        [_doneButton setTintColor:nil];
    }else{
        [_doneButton setEnabled:NO];
        [_doneButton setTintColor:[UIColor clearColor]];
    }
}
-(void)toggleButtonText{
    [_actionButton setTitle:[self buttonText] forState:UIControlStateNormal];
}
-(NSString *)buttonText{
    NSString *text;
    if (_theItem.isCheckedOut) {
        text = @"Check in";
    }else{
        text = @"Check out";
    }
    return text;
}
-(IBAction)buttonPressed:(id)sender{
    if (_theItem.isCheckedOut) {
        //check in action
        _theItem.isCheckedOut = NO;
        _theItem.owner = @"none";
        [_dataModel checkoutItemWithDictionary:[_theItem ItemDictionaryForCheckin] withCompletion:nil];
        [self refreshLabels];
        
    }else{
        //checkout action
        [self presentOwnerEntryViewWithMesage:nil];
    }
    
}
-(void)updateTableWithNotes{
    _theItem.notes = _notesTextview.text;
    [_dataModel updateItem:_theItem withCompletion:nil];
    [_notesTextview resignFirstResponder];
    [self showDoneButton:NO];
}
-(void)completeCheckoutWithOwner:(NSString *)owner{
    _theItem.isCheckedOut = YES;
    _theItem.checkedoutDate = [NSDate date];
    _theItem.owner = owner;
    [_dataModel checkoutItemWithDictionary:[_theItem ItemDictionaryForCheckout] withCompletion:nil];
    [self refreshLabels];
}
-(void)refreshLabels{
    _ownerText.text = _theItem.owner;
    if (_theItem.isCheckedOut == NO) {
        _isCheckedOutText.text = @"No";
        _dateText.text = @"";
    }else{
        _isCheckedOutText.text = @"Yes";
        _dateText.text = [InventoryItem convertDateToString:_theItem.checkedoutDate];
    }
    [self toggleButtonText];

    
}
-(void)setupLables{
    UIColor *labelColor = [UIColor whiteColor];
    NSArray *labels = @[_categoryText, _CategoryLabel, _nameText, _nameLabel, _ownerText, _ownerLabel, _isCheckedOutText, _isCheckedOutLabel, _dateText, _dateLabel, _notesLabel];
    for (UILabel *label in labels) {
        label.textColor = labelColor;
    }
    _notesTextview.textColor = labelColor;
    _notesTextview.backgroundColor = [UIColor themeColorNamed:kLightColor];
    _notesTextview.layer.cornerRadius = 10.0;
    _notesTextview.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    _CategoryLabel.text = @"Category:";
    _nameLabel.text = @"Name";
    _ownerLabel.text = @"Owner";
    _isCheckedOutLabel.text = @"Is Checked Out";
    _dateLabel.text = @"Date Checked Out";
    _notesLabel.text = @"Notes";
    
    _categoryText.text = _theItem.category;
    _nameText.text = _theItem.name;
    _ownerText.text = _theItem.owner;
    if([_theItem.notes isEqualToString:@"none"]){
        _notesTextview.text = @"";
    }else{
        _notesTextview.text = _theItem.notes;

    }
    
    if (_theItem.isCheckedOut == NO) {
        _isCheckedOutText.text = @"No";
        _dateText.text = @"";
    }else{
        _isCheckedOutText.text = @"Yes";
        _dateText.text = [InventoryItem convertDateToString:_theItem.checkedoutDate];
        
    }
    [_actionButton setTitle:[self buttonText] forState:UIControlStateNormal];

    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if(textView == _notesTextview){
        [self showDoneButton:YES];
    }
}
-(void)presentOwnerEntryViewWithMesage:(NSString *)message{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Checked out to:"
                                                                              message: message
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"name";
        textField.textColor = [UIColor blueColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        NSArray * textfields = alertController.textFields;
        UITextField * namefield = textfields[0];
        if ([namefield.text isEqualToString:@""]) {
            [self presentOwnerEntryViewWithMesage:@"Please Enter a Name"];
        }else{
            [self completeCheckoutWithOwner:namefield.text];
            
        }
        
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
