//
//  MasterViewController.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "InventorytListViewController.h"
#import "InventoryDetailViewController.h"
#import "ManualInsertViewController.h"
#import "ScannerViewController.h"
#import "DataModel.h"
#import "Constant.h"

@interface InventorytListViewController ()
@property (strong, nonatomic)DataModel *dataModel;
@property BOOL sortedByName;

@end

@implementation InventorytListViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _dataModel = [DataModel new];
    self.navigationItem.title = @"IT Inventory";
    _sortedByName = YES;
    [_sortButton setTitle:@"Sort by Owner" forState:UIControlStateNormal];

    self.inventoryDetailViewController = (InventoryDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    [_dataModel getLocalData];
    [self refreshWebData];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable)
                                                 name:@"dataUpdated"
                                               object:nil];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor themeColorNamed:kMidColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refreshWebData)
                  forControlEvents:UIControlEventValueChanged];
    self.theTableView.refreshControl = _refreshControl;
}
-(void)refreshWebData{
    [_dataModel refreshData:^
     {
         [self.theTableView reloadData];
         if(_refreshControl){
             [_refreshControl endRefreshing];
         }
     }];
}
- (void)viewWillAppear:(BOOL)animated {
 //   self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
   // [self.theTableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Segues
-(void)refreshTable{
    NSLog(@"table Refreshed");
    [self.theTableView reloadData];

}
-(void)toggleSortButton{
    NSString *title;
    if(_sortedByName){
        title = @"Sort by Item";
    }else{
        title = @"Sort by Owner";
    }
    [_sortButton setTitle:title forState:UIControlStateNormal];

    _sortedByName = !_sortedByName;
}
- (IBAction)sort:(id)sender {
    NSString *key;
    if(_sortedByName){
        key = @"owner";
    }else{
        key = @"name";
    }
    [self toggleSortButton];
    [_dataModel sortCategoryDataWithKey:key];

    [self refreshTable];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
  
        NSIndexPath *indexPath = [self.theTableView indexPathForSelectedRow];
        InventoryDetailViewController *controller = (InventoryDetailViewController *)[[segue destinationViewController] topViewController];
        
        NSString *key = [_dataModel.sortedCategories objectAtIndex:indexPath.section];
        NSArray *sectionItems = [_dataModel.sections objectForKey:key];
        InventoryItem *currentItem = [sectionItems objectAtIndex:indexPath.row];
        controller.theItem = currentItem;
        controller.dataModel = self.dataModel;
        controller.navigationItem.title = currentItem.IDString;
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
 
        
        [_theTableView deselectRowAtIndexPath:[_theTableView indexPathForSelectedRow] animated:YES];

    }
    if ([segue.destinationViewController isKindOfClass:[ScannerViewController class]]) {
        ScannerViewController *controller = (ScannerViewController *)[segue destinationViewController];
        controller.navigationItem.title = @"Scanner";
        controller.dataModel = self.dataModel;
        controller.listController = self;
    }
    if ([segue.destinationViewController isKindOfClass:[ManualInsertViewController class]]) {
        ManualInsertViewController *controller = (ManualInsertViewController *)[segue destinationViewController];
        controller.navigationItem.title = @"Add Item";
        controller.dataModel = self.dataModel;
    }

}
//used by scannerView Controller
-(void)dismissModalControllerAfterUpdatesWithItem:(InventoryItem *)item{
    [self dismissViewControllerAnimated:YES completion:^{
        //this could be a dataModel method
        NSInteger sectionChanged = [_dataModel sectionChangeFromItem:item];
        NSArray *sectionArray = [_dataModel.sections objectForKey:item.category];
        //find where new item is inserted
        int itemIndex = 0;
        for (int i = 0; i < sectionArray.count; i++) {
            InventoryItem *currentItem = [sectionArray objectAtIndex:i];
            if ([currentItem.IDString isEqualToString:item.IDString]) {
                itemIndex = i;
            }
        }

        NSArray *insertArray = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:itemIndex inSection:sectionChanged]];
        [self.theTableView beginUpdates];
        //check for a new section added to table
        if (_theTableView.numberOfSections < _dataModel.sections.count) {
            [self.theTableView insertSections:[NSIndexSet indexSetWithIndex:sectionChanged] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [self.theTableView insertRowsAtIndexPaths:insertArray withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.theTableView endUpdates];
        
    }];

}
-(void)dismissAfterCheckedStatusUpdate{
    [self dismissViewControllerAnimated:YES completion:nil];

}
- (IBAction)dismissAnyModal:(UIStoryboardSegue *)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)dismissManualInsertWithSegue:(UIStoryboardSegue *)sender {
    __block NSInteger sectionChanged = 0;
    __block NSString *key;
    __block InventoryItem *item;

    if ([sender.sourceViewController isKindOfClass:[ManualInsertViewController class]]) {
        ManualInsertViewController *sourceViewController = sender.sourceViewController;
        //insertion will be done in manualInsertViewController
        //delete this
        //[_dataModel insertNewItem:sourceViewController.theItem];
        
        sectionChanged = [_dataModel sectionChangeFromItem:sourceViewController.theItem];
        key = sourceViewController.theItem.category;
        item = sourceViewController.theItem;
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        //this could/should be a dataModel method
        NSInteger sectionChanged = [_dataModel sectionChangeFromItem:item];
        NSArray *sectionArray = [_dataModel.sections objectForKey:item.category];
        //find where new item is inserted
        int itemIndex = 0;
        for (int i = 0; i < sectionArray.count; i++) {
            InventoryItem *currentItem = [sectionArray objectAtIndex:i];
            if ([currentItem.IDString isEqualToString:item.IDString]) {
                itemIndex = i;
            }
        }
        NSArray *insertArray = [NSArray arrayWithObject:[NSIndexPath indexPathForRow:itemIndex inSection:sectionChanged]];
        
        [self.theTableView beginUpdates];
        //check for a new section added to table
        if (_theTableView.numberOfSections < _dataModel.sections.count) {
            [self.theTableView insertSections:[NSIndexSet indexSetWithIndex:sectionChanged] withRowAnimation:UITableViewRowAnimationAutomatic];
        }

        [self.theTableView insertRowsAtIndexPaths:insertArray withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.theTableView endUpdates];
        
    }];

}


#pragma mark - Table View
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 4.0f;
}
-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    header.textLabel.textColor = [UIColor blackColor];
    header.backgroundView.backgroundColor = [UIColor themeColorNamed:kLightColor];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor themeColorNamed:kLightColor];
    
    return view;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataModel.sortedCategories.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    NSString *key = [_dataModel.sortedCategories objectAtIndex:section];
    NSArray *sectionItems = [_dataModel.sections objectForKey:key];
    return sectionItems.count;

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_dataModel.sortedCategories objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    //get key for current section
    NSString *key = [_dataModel.sortedCategories objectAtIndex:indexPath.section];
    NSArray *sectionItems = [_dataModel.sections objectForKey:key];
    InventoryItem *currentItem = [sectionItems objectAtIndex:indexPath.row];
    
    cell.textLabel.text = currentItem.name;
    cell.backgroundColor = [UIColor themeColorNamed:kDarkColor];
    cell.textLabel.textColor =[UIColor whiteColor];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.text = [self checkedOutString:currentItem];
    return cell;
}
-(NSString *)checkedOutString:(InventoryItem *)item{
    NSString *str;
    if (item.isCheckedOut == NO) {
        str = @"Not Checked Out";
    }else{
        str = [NSString stringWithFormat:@"Checked out to: %@ - %@",item.owner, [InventoryItem convertDateToString: item.checkedoutDate]];
    }
    return str;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //get key for current section
        NSString *key = [_dataModel.sortedCategories objectAtIndex:indexPath.section];
        NSArray *sectionItems = [_dataModel.sections objectForKey:key];
        InventoryItem *currentItem = [sectionItems objectAtIndex:indexPath.row];
        NSInteger sectionChanged = [_dataModel sectionChangeFromItem:currentItem];

        BOOL lastItem = sectionItems.count == 1;
        [_dataModel deleteItemWithIDString:currentItem.IDString andCompletion:^{
            
            if (lastItem) {
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionChanged] withRowAnimation:UITableViewRowAnimationAutomatic];

            }else{
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

            }

        }];

    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}



@end
