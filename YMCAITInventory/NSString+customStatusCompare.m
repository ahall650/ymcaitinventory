//
//  NSString+customStatusCompare.m
//  YMCAITInventory
//
//  Created by anthony hall on 6/10/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "NSString+customStatusCompare.h"

@implementation NSString (customStatusCompare)
- (NSComparisonResult)customStatusCompare:(NSString*)other {
    NSAssert([other isKindOfClass:[NSString class]], @"Must be a NSString");
    if ([self isEqual:other]) {
        return NSOrderedSame;
    }
    else if ([self length] > 0 && [other length] > 0) {
        return [self localizedCaseInsensitiveCompare:other];
    }
    else if ([self length] > 0 && [other length] == 0) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedDescending;
    }
}
@end
