//
//  Constant.h
//  YMCAITInventory
//
//  Created by anthony hall on 4/2/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>



#import "UIColor+Colors.h"
#import "ThemeManager.h"

//theme keys
static NSString *kLightColor =     @"lightColor";
static NSString *kDarkColor =     @"darkColor";
static NSString *kMidColor =            @"midColor";
static NSString *kActionButtonColor =   @"actionButtonColor";
static NSString *kButtonTextColor =     @"buttonTextColor";

//defaults keys
static NSString *kTheme =               @"kTheme";

@interface Constant : NSObject

@end
