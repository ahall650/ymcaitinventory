//
//  ThemeManager.h
//  AHMileage
//
//  Created by anthony hall on 10/10/15.
//  Copyright © 2015 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThemeManager : NSObject
+ (ThemeManager *)sharedThemeManager;
@property (nonatomic, strong)NSDictionary *styles;
-(NSArray *)getThemeNames;
-(void)refreshView;
@end
