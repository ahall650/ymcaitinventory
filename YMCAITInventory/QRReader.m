//
//  QRReader.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/12/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "QRReader.h"
#import <AVFoundation/AVFoundation.h>
#import "InventoryItem.h"
@implementation QRReader
- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}
- (void)setupScanningSession {
    self.captureSession = [[AVCaptureSession alloc] init];
    
    NSError *error;
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    
    if (!input) {
        NSLog(@"Error Getting Camera Input");
        return;
    }
    [self.captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [self.captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("scanQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
    
    self.captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [_delegate setCaptureLayerFrameWithLayer:self.captureLayer];
}
#pragma mark - AVCaptureMetadataOutputObjectsDelegate method
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    NSString *capturedBarcode = nil;
    NSArray *supportedBarcodeTypes = @[AVMetadataObjectTypeUPCECode,
                                       AVMetadataObjectTypeCode39Code,
                                       AVMetadataObjectTypeCode39Mod43Code,
                                       AVMetadataObjectTypeEAN13Code,
                                       AVMetadataObjectTypeEAN8Code,
                                       AVMetadataObjectTypeCode93Code,
                                       AVMetadataObjectTypeCode128Code,
                                       AVMetadataObjectTypePDF417Code,
                                       AVMetadataObjectTypeQRCode,
                                       AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *barcodeMetadata in metadataObjects) {
        for (NSString *supportedBarcode in supportedBarcodeTypes) {
            
            if ([supportedBarcode isEqualToString:barcodeMetadata.type]) {
                AVMetadataMachineReadableCodeObject *barcodeObject = (AVMetadataMachineReadableCodeObject *)[self.captureLayer transformedMetadataObjectForMetadataObject:barcodeMetadata];
                capturedBarcode = [barcodeObject stringValue];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self.captureSession stopRunning];
                    [_delegate didFinishScanning:capturedBarcode];
                });
                return;
            }
        }
    }
   
}
//TODO need to actually get data from database for udpated notes and info
//just get name and then return the item from database
-(InventoryItem *)parseBarcodeData:(NSString *)data{
    NSMutableDictionary *itemDictionary = [[NSMutableDictionary alloc] init];
    //NSArray *components = [data componentsSeparatedByString:@"\n"];
    NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"\r\n"];
    NSArray *components = [data componentsSeparatedByCharactersInSet:set];
    
    for (NSString *keyValuePair in components)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@":"];
        NSString *key = [pairComponents firstObject];
        NSString *value = [pairComponents lastObject];
        [itemDictionary setObject:value forKey:key];

    }
    InventoryItem *newItem = [[InventoryItem alloc]initWithDictionary:itemDictionary];
    return newItem;
}

@end
