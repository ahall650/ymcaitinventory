//
//  SyncManager.m
//  YMCAInventory
//
//  Created by anthony hall on 3/11/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "SyncManager.h"
#import <MicrosoftAzureMobile/MicrosoftAzureMobile.h>
#import "InventoryItem.h"
@interface SyncManager()
@property (nonatomic, strong)   MSTable *table;

@end

@implementation SyncManager
+ (SyncManager *)sharedSyncManager
{
    static SyncManager* _sharedSyncManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedSyncManager = [[SyncManager alloc] init];
    });
    
    return _sharedSyncManager;
}
-(SyncManager *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        self.client = [MSClient clientWithApplicationURLString:@"https://inventory-ymca.azurewebsites.net"];

        
        // Create a Table instance 
        self.table = [_client tableWithName:@"InventoryYMCA"];

    }
    
    return self;
}
-(void)pullData:(ITCompletionBlock)completion{
    [_table readWithCompletion:^(MSQueryResult *result, NSError *error) {
        if(error) {
            NSLog(@"ERROR %@", error);
        } else {
            self.data = result.items;
            if ([result.items count] > 0) {
                NSLog(@"is checked out value: %@",[result.items[0]objectForKey:@"isCheckedOut" ]);

            }
        }
        if (completion != nil) {
            
            dispatch_async(dispatch_get_main_queue(), completion);
        }
    }];

}
-(void)addItem:(NSDictionary *)item completion:(SyncManager *)completion
{

    [_table insert:item completion:^(NSDictionary *result, NSError *error)
     {
         if(error) {
             NSLog(@"ERROR IS IN ADDING\n %@", error);
         } else {

         }
     }];

}
-(void)queryStringAttribute:(NSString *)attribute andValue:(NSString *)value withCompletion:(void(^)(InventoryItem *))completion
{
    
    __block InventoryItem *returnedItem = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(name = %@)", value];

    [_table readWithPredicate:predicate completion:^(MSQueryResult *result, NSError *error) {
        if(error) {
            NSLog(@"ERROR %@", error);
        } else {
            for(NSDictionary *item in result.items) {
                InventoryItem *theItem = [[InventoryItem alloc]initWithDictionary:item];
                returnedItem = theItem;

            }
            completion(returnedItem);


        }

    }];
}
//DETLE AFTER OTHER BELOW IS WORKING
-(void)updateItem:(InventoryItem *)item withCompletion:(ITCompletionBlock)completion{
    [_table update:[item itemDictionaryForPosting] completion:^(NSDictionary *result, NSError *error) {
        if(error) {
            NSLog(@"ERROR %@", error);
        } else {
            NSLog(@"Item: %@", [result objectForKey:@"text"]);
        }
        if (completion != nil) {
            completion();
        }
    }];

}
-(void)udpateItemWithDictionary:(NSDictionary *)dict withCompletion:(ITCompletionBlock)completion{
    [_table update:dict completion:^(NSDictionary *result, NSError *error) {
        if(error) {
            NSLog(@"ERROR %@", error);
        } else {
            NSLog(@"Item: %@", [result objectForKey:@"text"]);
        }
        if (completion != nil) {
            completion();
        }
    }];
    
}
-(void)deleteItemWithIDSting:(NSString *)string completion:(ITCompletionBlock)completion{
    [_table deleteWithId:string completion:^(id itemId, NSError *error) {
        if(error) {
            NSLog(@"ERROR %@", error);
        } else {
            
        }
        if (completion != nil) {
            completion();
        }
    }];

}
- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        NSLog(@"ERROR IS FROM METHOD %@", error);
    }
}
@end
