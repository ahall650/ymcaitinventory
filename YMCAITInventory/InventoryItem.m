//
//  InventoryItem.m
//  YMCAITInventory
//
//  Created by anthony hall on 3/18/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "InventoryItem.h"

@implementation InventoryItem

- (id)initWithDictionary:(NSDictionary*)dictionary {
    if (self = [super init]) {
        /*
        NSString *dateString = dictionary[@"checkedOutDate"];
        NSDate *date;
        if (![dateString isEqual:[NSNull null]]) {
            date = [self convertStringToDate:dictionary[@"checkedOutDate"]];
            self.checkedoutDate = date;

        }
         */
        
        self.category = dictionary[@"category"];
        self.name = dictionary[@"name"];
        self.IDString = dictionary[@"id"];
        self.isCheckedOut = [[dictionary objectForKey:@"isCheckedOut"]boolValue];
        self.owner = dictionary[@"owner"];
        self.checkedoutDate = dictionary[@"checkedOutDate"];
        self.notes = dictionary[@"notes"];
        if([self.owner isEqualToString:@"none"]){
            self.owner = @"";
        }
        
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.category = [decoder decodeObjectForKey:@"category"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.IDString = [decoder decodeObjectForKey:@"IDString"];
        self.owner = [decoder decodeObjectForKey:@"owner"];
        self.checkedoutDate = [decoder decodeObjectForKey:@"checkedOutDate"];
        self.isCheckedOut = [decoder decodeBoolForKey:@"isCheckedOut"];
        self.notes = [decoder decodeObjectForKey:@"notes"];
        if([self.owner isEqualToString:@"none"]){
            self.owner = @"";
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_category forKey:@"category"];
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeBool:_isCheckedOut forKey:@"isCheckedOut"];
    [encoder encodeObject:_IDString forKey:@"IDString"];
    [encoder encodeObject:_owner forKey:@"owner"];
    [encoder encodeObject:_checkedoutDate forKey:@"checkedOutDate"];
    [encoder encodeObject:_notes forKey:@"notes"];

}
-(NSDictionary *)ItemDictionaryForCheckout{
    return @{@"category":_category, @"name":_name, @"isCheckedOut":@(_isCheckedOut), @"notes":_notes, @"owner":_owner, @"checkedOutDate":_checkedoutDate, @"id":_IDString};
}
-(NSDictionary *)ItemDictionaryForCheckin{
    return @{@"category":_category, @"name":_name, @"isCheckedOut":@(_isCheckedOut), @"notes":_notes, @"owner":_owner, @"id" : _IDString, @"checkedOutDate": _checkedoutDate};
}

-(NSDictionary *)itemDictionaryForPosting{
   // NSString *theDate = [self convertDateToString:_checkedoutDate];
    if (self.owner == nil) {
        self.owner = @"";
    }
    return @{@"category":_category, @"name":_name, @"isCheckedOut":@(_isCheckedOut), @"notes":_notes, @"owner":_owner, @"id":_IDString};
}
+(NSString *)convertDateToString:(NSDate *)date{
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *dateString = [formatter stringFromDate:date];
    
    return dateString;
}

@end
