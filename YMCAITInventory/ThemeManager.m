//
//  ThemeManager.m
//  AHMileage
//
//  Created by anthony hall on 10/10/15.
//  Copyright © 2015 anthony hall. All rights reserved.
//

#import "ThemeManager.h"
#import "Constant.h"
@implementation ThemeManager
+ (ThemeManager *)sharedThemeManager
{
    static ThemeManager *_sharedThemeManager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        _sharedThemeManager = [ThemeManager new];
    });
    
    return _sharedThemeManager;
}
- (id)init
{
    if ((self = [super init]))
    {
        self.styles = [self getStyles];

    }
    return self;
}
-(void)refreshView{
    self.styles = [self getStyles];
    
    dispatch_async(dispatch_get_main_queue(),^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ThemeUpdated" object:nil];
    });

}
-(NSDictionary *)getStyles{
    
    NSString *themeName = @"Blue";
    NSURL *path = [[NSBundle mainBundle]
                   URLForResource:@"Themes" withExtension:@"plist"];
    NSDictionary *themes =
    [NSDictionary dictionaryWithContentsOfURL:path];
    return [themes objectForKey:themeName];
}
-(NSArray *)getThemeNames{
    NSURL *path = [[NSBundle mainBundle]
                   URLForResource:@"Themes" withExtension:@"plist"];
    NSDictionary *themes =
    [NSDictionary dictionaryWithContentsOfURL:path];
    return [themes objectForKey:@"themeNames"];
 
}
@end
