//
//  ITAlertView.m
//  YMCAITInventory
//
//  Created by anthony hall on 4/1/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import "ITAlertView.h"

@implementation ITAlertView
+(UIAlertController *)alertDuplicate{
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Duplicate Item"
                                                                              message: @"Enter a Unique Item Name"
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
    }]];
    return alertController;
}



@end
