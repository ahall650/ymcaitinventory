//
//  InventoryItem.h
//  YMCAITInventory
//
//  Created by anthony hall on 3/18/17.
//  Copyright © 2017 anthony hall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InventoryItem : NSObject <NSCoding>
@property(nonatomic, strong)NSString *category;
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSString *IDString;
@property BOOL isCheckedOut;
@property(nonatomic, strong)NSString *owner;
@property(nonatomic, strong)NSDate *checkedoutDate;
@property (nonatomic, strong)NSString *notes;
-(NSDictionary *)itemDictionaryForPosting;
- (id)initWithDictionary:(NSDictionary*)dictionary;
-(NSDictionary *)ItemDictionaryForCheckout;
-(NSDictionary *)ItemDictionaryForCheckin;

+(NSString *)convertDateToString:(NSDate *)date;
@end
